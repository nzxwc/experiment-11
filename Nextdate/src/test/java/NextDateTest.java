import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;

import java.util.Arrays;
import java.util.Collection;

import static org.junit.Assert.*;

@RunWith(Parameterized.class)
public class NextDateTest {

    public String expected;
    public String line;
    NextDate nextDate = new NextDate();
    public NextDateTest(String expected,String line){
        this.expected = expected;
        this.line = line;
    }

    @Parameterized.Parameters
    public static Collection<String[]> date(){
        return Arrays.asList(new String[][]{//简化后的决策表测试用例表
                //{"日期输入错误","2001年6月31日"},
                //{"日期输入错误","2001年2月29日"},
                {"日期输入错误","2001年2月30日"},
                {"日期输入错误","2020年2月0日"},
                {"日期输入错误","1937年2月30日"},
                {"日期输入错误","2060年7月1日"},
                {"日期输入错误","2001年4月31日"},
                {"日期输入错误","2001年4月0日"},
                {"日期输入错误","2001年7月-1日"},
                {"日期输入错误","2001"},
                {"日期输入错误","2001年-2月30日"},
                {"日期输入错误","2001年7月32日"},
                {"日期输入错误","2000年2月30日"},
                {"日期输入错误","2001年2月-2日"},
                {"日期输入错误","2001年13月30日"},
                {"日期输入错误","2050年12月31日"},
                //{"2001年6月17日","2001年6月16日"},
                {"2001年7月1日","2001年6月30日"},
                //{"2001年1月17日","2001年1月16日"},
                {"2001年2月1日","2001年1月31日"},
                {"2001年12月17日","2001年12月16日"},
                {"2002年1月1日","2001年12月31日"},
                {"2001年2月17日","2001年2月16日"},
                {"2004年2月29日","2004年2月28日"},
                {"2001年3月1日","2001年2月28日"},
                {"2004年9月30日","2004年9月29日"},
                {"2004年11月30日","2004年11月29日"},
                {"2004年3月1日","2004年2月29日"},
                //{"2004年4月30日","2004年4月29日"}

        });
    }

    @Test
    public void nextDate() {
        assertEquals(expected,nextDate.nextDate(line));
    }
}
