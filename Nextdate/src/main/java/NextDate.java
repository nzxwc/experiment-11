public class NextDate {

    public String nextDate(String line){
        //处理字符串
        line = line.replace('年',',');
        line = line.replace('月',',');
        line = line.replace('日',',');
        String[] str = line.split(",");
        if (str.length<3){
            return "日期输入错误";
        }
        Integer year = Integer.valueOf(str[0]);
        Integer month = Integer.valueOf(str[1]);
        Integer day = Integer.valueOf(str[2]);
        if (year < 1964 || year>2050) {
            return "日期输入错误";
        }

        if (month < 1 || month > 12) {
            return "日期输入错误";
        }

        if (month == 12 && day == 31 ) {
            if (year == 2050){
                return "日期输入错误";
            }else {
                return (year + 1) + "年1月1日";
            }
        }
//        // 判断输入的年份是平年还是闰年,来判断当输入2月时天数是否有误
        if ((year % 4 == 0 && year % 100 != 0) || year % 400 == 0) {// 该年是闰年,2月有29天
            if (month == 2) {
                if (day > 29) {
                    return "日期输入错误";
                } else if (day < 1) {
                    return "日期输入错误";
                } else {
                    if (day == 29) {
                        return year + "年" + 3 + "月" + 1+"日";
                    } else {
                        day += 1;
                    }
                }

            }
        } else {// 该年是平年,2月有28天
            if (month == 2) {
                if (day > 28) {
                    return "日期输入错误";
                } else if (day < 1) {
                    return "日期输入错误";
                } else {
                    if (day == 28) {
                        return year + "年" + 3 + "月" + 1+"日";
                    } else {
                        day += 1;
                    }
                }
            }
        }

        // 判断4 6 9 11 月的天数输入
        if (month == 4 || month == 6 || month == 9 || month == 11) {
            if (day > 30) {
                return "日期输入错误";
            } else if (day < 1) {
                return "日期输入错误";
            } else {
                if (day == 30) {
                    month += 1;
                    day = 1;
                } else {
                    day += 1;
                }
            }
        } else if (month != 2) { // 判断1 3 5 7 8 10 12月的天数输入
            if (day > 31) {
                return "日期输入错误";
            } else if (day < 1) {
                return "日期输入错误";
            } else {
                if (day == 31) {
                    month += 1;
                    day = 1;
                } else {
                    day += 1;
                }
            }
        }
        return year+"年"+month+"月"+day+"日";
    }
}


